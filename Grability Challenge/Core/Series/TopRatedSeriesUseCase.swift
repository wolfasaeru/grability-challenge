//
//  TopRatedSeriesUseCase.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/11/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import PromiseKit

class TopRatedSeriesUseCase {
    
    private let seriesRepository: SeriesRepository
    private var page: Int = 1
    
    init(seriesRepository: SeriesRepository) {
        
        self.seriesRepository = seriesRepository
    }
    
    func execute(isFirstPage: Bool = false) -> Promise<[Serie]> {
        self.page = (isFirstPage) ? 1 : self.page + 1
        
        return seriesRepository.series(in: .topRated, page: self.page)
    }
}
