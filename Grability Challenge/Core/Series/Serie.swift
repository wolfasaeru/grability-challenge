//
//  Serie.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/11/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import SwiftyJSON

class Serie {
    
    let id: Int
    let name: String
    let language: String
    let firstAirDate: Date
    let posterPath: String
    let overview: String
    let voteCount: Int
    let voteAvg: Double
    
    init(id: Int, name: String, language: String, firstAirDate: Date, posterPath: String, overview: String, voteCount: Int, voteAvg: Double) {
        
        self.id = id
        self.name = name
        self.language = language
        self.firstAirDate = firstAirDate
        self.posterPath = posterPath
        self.overview = overview
        self.voteCount = voteCount
        self.voteAvg = voteAvg
    }
}

class SerieMapper {
    
    static func map(from json: JSON) -> Serie? {
        
        return Serie(
            id: json["id"].intValue,
            name: json["name"].stringValue,
            language: json["original_language"].stringValue,
            firstAirDate: Date.fromApiDate(with: json["first_air_date"].stringValue),
            posterPath: json["poster_path"].stringValue,
            overview: json["overview"].stringValue,
            voteCount: json["vote_count"].intValue,
            voteAvg: json["vote_average"].doubleValue
        )
    }
}
