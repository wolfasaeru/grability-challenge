//
//  SeriesCoreServiceLocator.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/11/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

class SeriesCoreServiceLocator: ServiceLocatorModule {
    
    func registerServices(serviceLocator: ServiceLocator) {
        
        serviceLocator.register { self.provideSeriesRepository() }
        
        // Use Cases
        serviceLocator.register { self.providePopularSeriesUseCase() }
        serviceLocator.register { self.provideTopRatedSeriesUseCase() }
    }
    
    private func provideSeriesRepository() -> SeriesRepository {
        return SeriesRepository()
    }
    
    // MARK: Use Cases
    private func providePopularSeriesUseCase() -> PopularSeriesUseCase {
        return PopularSeriesUseCase(seriesRepository: ServiceLocator.inject())
    }
    
    private func provideTopRatedSeriesUseCase() -> TopRatedSeriesUseCase {
        return TopRatedSeriesUseCase(seriesRepository: ServiceLocator.inject())
    }
}
