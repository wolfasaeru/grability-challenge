//
//  SeriesRepository.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/11/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire
import SwiftyJSON

class SeriesRepository {
    
    private let queue = DispatchQueue.global()
    
    func series(in category: SeriesCategory, page: Int) -> Promise<[Serie]> {
        
        if let seriesDAO = SeriesDAO() {
            if !isInternetAvailable() && page == 1 {
                return seriesDAO.series(category)
            } else if page == 1 {
                seriesDAO.clear(category)
            } else if !isInternetAvailable() {
                return Promise<[Serie]> { fulfill, _ in
                    fulfill([])
                }
            }
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        return firstly { _ in
            Alamofire.request(self.buildRequest(for: category, page: page)).responseJSON()
            }.then(on: queue) { data in
                self.convertToMovies(from: data, in: category, for: page)
            }.always {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    private func buildRequest(for category: SeriesCategory, page: Int) -> APIRequestConvertible {
        switch category {
        case .popular:
            return SeriesRouter.popular(page: page).requestConvertible
        case .topRated:
            return SeriesRouter.topRated(page: page).requestConvertible
        }
    }
    
    private func convertToMovies(from data: Any, in category: SeriesCategory, for page: Int) -> [Serie] {
        
        let jsonResponse = JSON(data)
        
        guard let seriesJSON = jsonResponse["results"].array else { return [] }
        
        let series = seriesJSON.flatMap { SerieMapper.map(from: JSON($0.dictionaryValue)) }
        
        if let seriesDAO = SeriesDAO() {
            seriesDAO.add(series, in: category, for: page)
        }
        
        return series
    }
}
