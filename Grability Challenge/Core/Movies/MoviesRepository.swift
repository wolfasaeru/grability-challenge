//
//  MoviesRepository.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/10/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire
import SwiftyJSON

class MoviesRepository {
    
    private let queue = DispatchQueue.global()
    
    func movies(in category: MoviesCategory, page: Int) -> Promise<[Movie]> {
        
        if let moviesDAO = MoviesDAO() {
            if !isInternetAvailable() && page == 1 {
                return moviesDAO.movies(category)
            } else if page == 1 {
                moviesDAO.clear(category)
            } else if !isInternetAvailable() {
                return Promise<[Movie]> { fulfill, _ in
                    fulfill([])
                }
            }
        }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        return firstly { _ in
            Alamofire.request(buildRequest(for: category, page: page)).responseJSON()
        }.then(on: queue) { data in
            self.convertToMovies(from: data, in: category, for: page)
        }.always {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    private func buildRequest(for category: MoviesCategory, page: Int) -> APIRequestConvertible {
        switch category {
        case .popular:
            return MoviesRouter.popular(page: page).requestConvertible
        case .topRated:
            return MoviesRouter.topRated(page: page).requestConvertible
        case .upcoming:
            return MoviesRouter.upcoming(page: page).requestConvertible
        }
    }
    
    private func convertToMovies(from data: Any, in category: MoviesCategory, for page: Int) -> [Movie] {

        let jsonResponse = JSON(data)
        
        guard let moviesJSON = jsonResponse["results"].array else { return [] }
        
        let movies = moviesJSON.flatMap { MovieMapper.map(from: JSON($0.dictionaryValue)) }
        
        if let moviesDAO = MoviesDAO() {
            moviesDAO.add(movies, in: category, for: page)
        }
        
        return movies
    }
}
