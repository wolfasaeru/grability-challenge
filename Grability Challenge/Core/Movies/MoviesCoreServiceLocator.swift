//
//  MoviesCoreServiceLocator.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/11/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

class MoviesCoreServiceLocator: ServiceLocatorModule {
    
    func registerServices(serviceLocator: ServiceLocator) {
        
        serviceLocator.register { self.provideMoviesRepository() }
        
        // Inject Use Cases
        serviceLocator.register { self.providePopularMoviesUseCase() }
        serviceLocator.register { self.provideTopRatedMoviesUseCase() }
        serviceLocator.register { self.provideUpcomingMoviesUseCase() }
    }
    
    private func provideMoviesRepository() -> MoviesRepository {
        return MoviesRepository()
    }

    // MARK: Use Cases
    private func providePopularMoviesUseCase() -> PopularMoviesUseCase {
        return PopularMoviesUseCase(moviesRepository: ServiceLocator.inject())
    }
    
    private func provideTopRatedMoviesUseCase() -> TopRatedMoviesUseCase {
        return TopRatedMoviesUseCase(moviesRepository: ServiceLocator.inject())
    }
    
    private func provideUpcomingMoviesUseCase() -> UpcomingMoviesUseCase {
        return UpcomingMoviesUseCase(moviesRepository: ServiceLocator.inject())
    }
}
