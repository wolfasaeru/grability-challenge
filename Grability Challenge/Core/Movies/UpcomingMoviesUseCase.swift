//
//  UpcomingMoviesUseCase.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/11/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import PromiseKit

class UpcomingMoviesUseCase {
    
    private let moviesRepository: MoviesRepository
    private var page: Int = 1
    
    init(moviesRepository: MoviesRepository) {
        
        self.moviesRepository = moviesRepository
    }
    
    func execute(isFirstPage: Bool = false) -> Promise<[Movie]> {
        self.page = (isFirstPage) ? 1 : self.page + 1
     
        return moviesRepository.movies(in: .upcoming, page: self.page)
    }
}
