//
//  Movie.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/10/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import SwiftyJSON

class Movie {
    
    let id: Int
    let title: String
    let language: String
    let releaseDate: Date
    let posterPath: String
    let overview: String
    let voteCount: Int
    let voteAvg: Double
    
    init(id: Int, title: String, language: String, releaseDate: Date, posterPath: String, overview: String, voteCount: Int, voteAvg: Double) {
        
        self.id = id
        self.title = title
        self.language = language
        self.releaseDate = releaseDate
        self.posterPath = posterPath
        self.overview = overview
        self.voteCount = voteCount
        self.voteAvg = voteAvg
    }
}

class MovieMapper {
    
    static func map(from json: JSON) -> Movie? {
        
        return Movie(
            id: json["id"].intValue,
            title: json["title"].stringValue,
            language: json["original_language"].stringValue,
            releaseDate: Date.fromApiDate(with: json["release_date"].stringValue),
            posterPath: json["poster_path"].stringValue,
            overview: json["overview"].stringValue,
            voteCount: json["vote_count"].intValue,
            voteAvg: json["vote_average"].doubleValue
        )
    }
}
