//
//  MoviesDAO.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/14/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import PromiseKit
import CoreData

import JSQCoreDataKit

class MoviesDAO {

    private let stack: CoreDataStack
    private let backgroundChildContext: ChildContext
    
    init?() {
        guard CoreDataConfiguration.shared.stack != nil else { return nil }
        
        self.stack = CoreDataConfiguration.shared.stack
        self.backgroundChildContext = stack.childContext(concurrencyType: .privateQueueConcurrencyType)
    }
    
    func movies(_ category: MoviesCategory) -> Promise<[Movie]> {
        
        return Promise<[Movie]> { fulfill, failure in
            
            self.backgroundChildContext.performAndWait {
                do {
                    let fetch = MovieDTO.fetchRequest
                    
                    switch category {
                    case .popular:
                        fetch.predicate = NSPredicate(format: "orderPopular != %d", 0)
                    case .topRated:
                        fetch.predicate = NSPredicate(format: "orderTopRated != %d", 0)
                    case .upcoming:
                        fetch.predicate = NSPredicate(format: "orderUpcoming != %d", 0)
                    }
                    
                    let movies = try self.backgroundChildContext.fetch(fetch)
                    
                    fulfill(movies.map {
                        Movie(
                            id: Int($0.id),
                            title: $0.title,
                            language: $0.language,
                            releaseDate: $0.releaseDate as Date,
                            posterPath: $0.posterPath,
                            overview: $0.overview,
                            voteCount: Int($0.voteCount),
                            voteAvg: $0.voteAvg
                        )
                    })
                    
                } catch {
                    debugPrint("Error deleting objects: \(error)")
                    failure(error)
                }
            }
        }
        
    }
    
    func add(_ movies: [Movie], in category: MoviesCategory, for page: Int) {
        
        self.backgroundChildContext.performAndWait {
            do {
                let ids: [Int32] = movies.map { Int32($0.id) }
                
                let fetch = MovieDTO.fetchRequest
                
                switch category {
                case .popular:
                    fetch.predicate = NSPredicate(format: "id IN %@ AND orderPopular = %d", ids, 0)
                case .topRated:
                    fetch.predicate = NSPredicate(format: "id IN %@ AND orderTopRated = %d", ids, 0)
                case .upcoming:
                    fetch.predicate = NSPredicate(format: "id IN %@ AND orderUpcoming = %d", ids, 0)
                }
                
                let dbMovies = try self.backgroundChildContext.fetch(fetch)
                let dbIds = dbMovies.map { $0.id }
                
                let multiplier = (page - 1) * 10
                movies.enumerated().forEach { index, movie in
                    
                    if let existingId = dbIds.index(of: Int32(movie.id)) {
                        
                        let movieDTO = dbMovies.filter { $0.id == ids[existingId] }.first
                        
                        movieDTO?.orderPopular = Int16(index + 1)
                    } else {
                        let order = Int16(multiplier + index + 1)
                        
                        _ = MovieDTO(
                            context: self.backgroundChildContext,
                            id: Int32(movie.id),
                            title: movie.title,
                            language: movie.language,
                            releaseDate: movie.releaseDate as NSDate,
                            posterPath: movie.posterPath,
                            overview: movie.overview,
                            voteCount: Int16(movie.voteCount),
                            voteAvg: movie.voteAvg,
                            orderPopular: (category == .popular) ? order : 0,
                            orderTopRated: (category == .topRated) ? order : 0,
                            orderUpcoming: (category == .upcoming) ? order : 0
                        )
                    }
                }
                
                saveContext(self.backgroundChildContext)
            } catch {
                debugPrint("Error insering objects: \(error)")
            }
        }
    }
    
    func clear(_ category: MoviesCategory) {

        self.backgroundChildContext.performAndWait {
            do {
                let fetch = MovieDTO.fetchRequest
                
                switch category {
                case .popular:
                    fetch.predicate = NSPredicate(format: "orderPopular != %@", 0)
                case .topRated:
                    fetch.predicate = NSPredicate(format: "orderTopRated != %@", 0)
                case .upcoming:
                    fetch.predicate = NSPredicate(format: "orderUpcoming != %@", 0)
                }
                
                let movies = try self.backgroundChildContext.fetch(fetch)
                
                for movie in movies {
                    self.backgroundChildContext.delete(movie)
                }
                
                saveContext(self.backgroundChildContext)
            } catch {
                print("Error deleting objects: \(error)")
            }
        }
    }
}
