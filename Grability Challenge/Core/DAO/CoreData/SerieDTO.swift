//
//  SerieDTO+CoreDataClass.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/14/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import CoreData
import JSQCoreDataKit

public final class SerieDTO: NSManagedObject, CoreDataEntityProtocol {
    
    public static let defaultSortDescriptors = [
        NSSortDescriptor(key: "orderPopular", ascending: true),
        NSSortDescriptor(key: "orderTopRated", ascending: true)
    ]

    @NSManaged public var id: Int32
    @NSManaged public var name: String
    @NSManaged public var language: String
    @NSManaged public var firstAirDate: NSDate
    @NSManaged public var posterPath: String
    @NSManaged public var overview: String
    @NSManaged public var voteCount: Int16
    @NSManaged public var voteAvg: Double
    @NSManaged public var orderPopular: Int16
    @NSManaged public var orderTopRated: Int16
    
    public init(
        context: NSManagedObjectContext,
        id: Int32,
        name: String,
        language: String,
        firstAirDate: NSDate,
        posterPath: String,
        overview: String,
        voteCount: Int16,
        voteAvg: Double,
        orderPopular: Int16,
        orderTopRated: Int16
    ) {
        super.init(entity: SerieDTO.entity(context: context), insertInto: context)
        
        self.id = id
        self.name = name
        self.language = language
        self.firstAirDate = firstAirDate
        self.posterPath = posterPath
        self.overview = overview
        self.voteCount = voteCount
        self.voteAvg = voteAvg
        self.orderPopular = orderPopular
        self.orderTopRated = orderTopRated
    }
    
    @objc
    private override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }
}
