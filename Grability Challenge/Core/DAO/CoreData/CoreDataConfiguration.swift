//
//  CoreDataConfiguration.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/14/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

import JSQCoreDataKit
import PromiseKit

class CoreDataConfiguration {
    
    static let shared = CoreDataConfiguration()
    
    var stack: CoreDataStack!
    
    static func setupStack() -> Promise<Void> {
        return shared.setupStack()
    }
    
    func setupStack() -> Promise<Void> {
        
        return Promise<Void> { fulfill, failure in
            
            let model = CoreDataModel(name: MoviesDBConfiguration.modelName, bundle: .main)
            let factory = CoreDataStackFactory(model: model)
            
            factory.createStack { (result: StackResult) -> Void in
                switch result {
                case .success(let s):
                    self.stack = s
                    fulfill()
                    
                case .failure(let err):
                    assertionFailure("Error creating stack: \(err)")
                    failure(err)
                }
            }
        }
    }
    
}
