//
//  SeriesDAO.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/15/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import PromiseKit
import CoreData

import JSQCoreDataKit

class SeriesDAO {
    
    private let stack: CoreDataStack
    private let backgroundChildContext: ChildContext
    
    init?() {
        guard CoreDataConfiguration.shared.stack != nil else { return nil }
        
        self.stack = CoreDataConfiguration.shared.stack
        self.backgroundChildContext = stack.childContext(concurrencyType: .privateQueueConcurrencyType)
    }
    
    func series(_ category: SeriesCategory) -> Promise<[Serie]> {
        
        return Promise<[Serie]> { fulfill, failure in
            
            self.backgroundChildContext.performAndWait {
                do {
                    let fetch = SerieDTO.fetchRequest
                    
                    switch category {
                    case .popular:
                        fetch.predicate = NSPredicate(format: "orderPopular != %d", 0)
                    case .topRated:
                        fetch.predicate = NSPredicate(format: "orderTopRated != %d", 0)
                    }
                    
                    let series = try self.backgroundChildContext.fetch(fetch)
                    
                    fulfill(series.map {
                        Serie(
                            id: Int($0.id),
                            name: $0.name,
                            language: $0.language,
                            firstAirDate: $0.firstAirDate as Date,
                            posterPath: $0.posterPath,
                            overview: $0.overview,
                            voteCount: Int($0.voteCount),
                            voteAvg: $0.voteAvg
                        )
                    })
                    
                } catch {
                    debugPrint("Error deleting objects: \(error)")
                    failure(error)
                }
            }
        }
        
    }
    
    func add(_ series: [Serie], in category: SeriesCategory, for page: Int) {
        
        self.backgroundChildContext.performAndWait {
            do {
                let ids: [Int32] = series.map { Int32($0.id) }
                
                let fetch = SerieDTO.fetchRequest
                
                switch category {
                case .popular:
                    fetch.predicate = NSPredicate(format: "id IN %@ AND orderPopular = %d", ids, 0)
                case .topRated:
                    fetch.predicate = NSPredicate(format: "id IN %@ AND orderTopRated = %d", ids, 0)
                }
                
                let dbSeries = try self.backgroundChildContext.fetch(fetch)
                let dbIds = dbSeries.map { $0.id }
                
                let multiplier = (page - 1) * 10
                series.enumerated().forEach { index, serie in
                    
                    if let existingId = dbIds.index(of: Int32(serie.id)) {
                        
                        let serieDTO = dbSeries.filter { $0.id == ids[existingId] }.first
                        
                        serieDTO?.orderPopular = Int16(index + 1)
                    } else {
                        let order = Int16(multiplier + index + 1)
                        
                        _ = SerieDTO(
                            context: self.backgroundChildContext,
                            id: Int32(serie.id),
                            name: serie.name,
                            language: serie.language,
                            firstAirDate: serie.firstAirDate as NSDate,
                            posterPath: serie.posterPath,
                            overview: serie.overview,
                            voteCount: Int16(serie.voteCount),
                            voteAvg: serie.voteAvg,
                            orderPopular: (category == .popular) ? order : 0,
                            orderTopRated: (category == .topRated) ? order : 0
                        )
                    }
                }
                
                saveContext(self.backgroundChildContext)
            } catch {
                debugPrint("Error insering objects: \(error)")
            }
        }
    }
    
    func clear(_ category: SeriesCategory) {
        
        self.backgroundChildContext.performAndWait {
            do {
                let fetch = SerieDTO.fetchRequest
                
                switch category {
                case .popular:
                    fetch.predicate = NSPredicate(format: "orderPopular != %@", 0)
                case .topRated:
                    fetch.predicate = NSPredicate(format: "orderTopRated != %@", 0)
                }
                
                let series = try self.backgroundChildContext.fetch(fetch)
                
                for serie in series {
                    self.backgroundChildContext.delete(serie)
                }
                
                saveContext(self.backgroundChildContext)
            } catch {
                print("Error deleting objects: \(error)")
            }
        }
    }
}
