//
//  MoviesRouter.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/10/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import Alamofire

enum MoviesRouter: APIRouter {
    static let basePath = "movie"
    
    case popular(page: Int)
    case topRated(page: Int)
    case upcoming(page: Int)
    case image(imagePath: String)
    
    var method: HTTPMethod {
        
        switch self {
        case .popular, .topRated, .upcoming, .image:
            return .get
        }
    }
    
    var encoding: Alamofire.ParameterEncoding {
        return Alamofire.URLEncoding.default
    }
    
    var path: String {
        switch self {
        case .popular:
            return "\(MoviesRouter.basePath)/popular"
        case .topRated:
            return "\(MoviesRouter.basePath)/top_rated"
        case .upcoming:
            return "\(MoviesRouter.basePath)/upcoming"
        case .image(let imgPath):
            return imgPath
        }
    }
    
    var parameters: [String: AnyObject]? {

        var params = ["api_key": MoviesDBConfiguration.apiKey as AnyObject]
        
        switch self {
        case .popular(let page), .topRated(let page), .upcoming(let page):
            params["page"] = page as AnyObject
        default:
            return params
        }

        return params
    }
    
    var requestConvertible: APIRequestConvertible {
        return APIRequestConvertible(router: self)
    }
}
