//
//  MoviesDBConfiguration.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/10/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

struct MoviesDBConfiguration {
    
    enum ImgSizePath: String {
        case thumbnail = "w92"
        case medium = "w185"
        case original = "original"
    }
    
    static let apiKey = "cc5b7d6c93e7bb0a639254d776648c86"
    static let apiUrl = "https://api.themoviedb.org/3"
    static let apiImageUrl = "http://image.tmdb.org/t/p"
    
    static let modelName = "Grability_Challenge"
}
