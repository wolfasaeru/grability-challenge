//
//  SeriesRouter.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/10/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import Alamofire

enum SeriesRouter: APIRouter {
    static let basePath = "tv/"
    
    case popular(page: Int)
    case topRated(page: Int)
    
    var method: HTTPMethod {
        
        switch self {
        case .popular, .topRated:
            return .get
        }
    }
    
    var encoding: Alamofire.ParameterEncoding {
        return Alamofire.URLEncoding.default
    }
    
    var path: String {
        switch self {
        case .popular:
            return "\(SeriesRouter.basePath)popular"
        case .topRated:
            return "\(SeriesRouter.basePath)top_rated"
        }
    }
    
    var parameters: [String: AnyObject]? {
        var params = ["api_key": MoviesDBConfiguration.apiKey as AnyObject]
        
        switch self {
        case .popular(let page), .topRated(let page):
            params["page"] = page as AnyObject
        }
        
        return params
    }
    
    var requestConvertible: APIRequestConvertible {
        return APIRequestConvertible(router: self)
    }
}
