//
//  Router.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/10/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import Alamofire

// MARK: Router
protocol APIRouter {
    
    var method: Alamofire.HTTPMethod { get }
    var encoding: Alamofire.ParameterEncoding { get }
    var path: String { get }
    var parameters: [String: AnyObject]? { get }
    var requestConvertible: APIRequestConvertible { get }
}

// MARK: - Request
struct APIRequestConvertible: URLRequestConvertible {
    
    private static let baseURLString: NSURL = NSURL(string: MoviesDBConfiguration.apiUrl)!

    let router: APIRouter
    
    var oAuthToken: String?
    
    init(router: APIRouter) {
        self.router = router
    }
    
    func asURLRequest() throws -> URLRequest {
        var request = URLRequest(url: APIRequestConvertible.baseURLString.appendingPathComponent(router.path)!)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (_, _, error) in
            if error != nil {
                return
            }
        }
        
        task.resume()
        
        request.httpMethod = router.method.rawValue
        request.timeoutInterval = TimeInterval(10 * 1000)
        
        // Left this here - makes easier the upgrade to TheMoviesDB API v4
        if let token = oAuthToken {
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        
        guard let parameters = router.parameters else { return request }
        
        return try router.encoding.encode(request, with: parameters)
    }
    
}
