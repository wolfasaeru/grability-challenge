//
//  ChallengeNavigationController.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/12/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit

class ChallengeNavigationController: UINavigationController {
    
    @IBInspectable var clearBackTitle: Bool = true
    
    override open func pushViewController(_ viewController: UIViewController, animated: Bool) {
        performClearBackTitle()
        super.pushViewController(viewController, animated: animated)
    }
    
    override open func show(_ viewController: UIViewController, sender: Any?) {
        performClearBackTitle()
        super.show(viewController, sender: sender)
    }
    
    private func performClearBackTitle() {
        if clearBackTitle {
            topViewController?.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
    }
    
}
