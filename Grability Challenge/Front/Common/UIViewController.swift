//
//  UIViewController.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/9/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit
import SVProgressHUD

extension UIViewController {
    
    func showLoadingView() {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show(withStatus: translate("Espere ..."))
    }
    
    func hideLoadingView() {
        SVProgressHUD.dismiss()
    }
}
