//
//  UIScrollView.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/11/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit

extension UIScrollView {

    /// listen to keyboard event
    func makeAwareOfKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyBoardDidHide), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    /// remove observer
    func removeKeyboardObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    /// move scroll view up
    func keyBoardDidShow(notification: Notification) {
        guard let rectValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue else { return }

        let keyboardSize = rectValue.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        
        self.contentInset = contentInsets
        self.scrollIndicatorInsets = contentInsets
    }

    /// move scrollview back down
    func keyBoardDidHide(notification: Notification) {
        // restore content inset to 0
        self.contentInset = UIEdgeInsets.zero
        self.scrollIndicatorInsets = UIEdgeInsets.zero
    }
}
