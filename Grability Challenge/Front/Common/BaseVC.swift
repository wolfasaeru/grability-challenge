//
//  BaseVC.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/9/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit

protocol BaseView: class {
    
    func dismissKeyboard()
    func showLoadingView()
    func hideLoadingView()
    
    func showAlert(with error: Error)
}

class BaseVC: UIViewController {
    
    var toolbarHidden: Bool = true
    
    var presenter: Presenter! {
        return nil
    }
    
    var challengeNavigationController: ChallengeNavigationController? {
        return navigationController as? ChallengeNavigationController
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.darkGrayBackground
        
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setToolbarHidden(toolbarHidden, animated: animated)
        presenter.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
}

extension BaseVC: BaseView {
    
    func dismissKeyboard() {
        guard let currentFirstResponder = view.currentFirstResponder() else { return }
        currentFirstResponder.resignFirstResponder()
    }
    
    func showAlert(with error: Error) {
        let alertViewController = UIAlertController(title: translate("Error"), message: error.localizedDescription, preferredStyle: .alert)
        alertViewController.addAction(UIAlertAction(title: translate("Ok"), style: .default, handler: nil))
        present(alertViewController, animated: true, completion: nil)
    }
}
