//
//  UIView.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/9/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit

extension UIView {
    
    var originX: CGFloat {
        get {
            return self.frame.origin.x
        } set (value) {
            self.frame = CGRect (x: value, y: originY, width: width, height: self.height)
        }
    }
    
    var originY: CGFloat {
        get {
            return self.frame.origin.y
        } set (value) {
            self.frame = CGRect (x: originX, y: value, width: width, height: height)
        }
    }
    
    var width: CGFloat {
        get {
            return self.frame.size.width
        } set (value) {
            self.frame = CGRect (x: self.originX, y: originY, width: value, height: height)
        }
    }
    
    var height: CGFloat {
        get {
            return self.frame.size.height
        } set (value) {
            self.frame = CGRect (x: originX, y: originY, width: width, height: value)
        }
    }
    
    var left: CGFloat {
        get {
            return originX
        } set (value) {
            originX = value
        }
    }
    
    var right: CGFloat {
        get {
            return originX + width
        } set (value) {
            originX = value - width
        }
    }
    
    var top: CGFloat {
        get {
            return originY
        } set (value) {
            originY = value
        }
    }
    
    var bottom: CGFloat {
        get {
            return originY + height
        } set (value) {
            originY = value - height
        }
    }
    
    func leftWithOffset (_ offset: CGFloat) -> CGFloat {
        return self.left - offset
    }
    
    func rightWithOffset (_ offset: CGFloat) -> CGFloat {
        return self.right + offset
    }
    
    func topWithOffset (_ offset: CGFloat) -> CGFloat {
        return self.top - offset
    }
    
    func bottomWithOffset (_ offset: CGFloat) -> CGFloat {
        return self.bottom + offset
    }
    
    func cornerRadius(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func currentFirstResponder() -> UIResponder? {
        
        if isFirstResponder {
            return self
        }
        
        for view in self.subviews {
            if let responder = view.currentFirstResponder() {
                return responder
            }
        }
        
        return nil
    }
    
    func addBorder(_ width: CGFloat, color: UIColor) {
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        self.layer.masksToBounds = true
    }
}
