//
//  Presenter.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/9/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

protocol Presenter {
    
    func viewDidLoad()
    func viewWillAppear()
    func viewDidAppear()
}

extension Presenter {
    
    // Empty default implementation
    // If you need do something, you can overwrite these functions in your presenter
    func viewDidLoad() { }
    func viewWillAppear() { }
    func viewDidAppear() { }
}
