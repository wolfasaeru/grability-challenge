//
//  UIFont.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/11/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit

extension UIFont {
    
    class func regular(size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: UIFontWeightRegular)
    }
    
    class func bold(size: CGFloat) -> UIFont {
        return UIFont.systemFont(ofSize: size, weight: UIFontWeightBold)
    }
    
    class func semiBold(size: CGFloat) -> UIFont {
        return  UIFont.systemFont(ofSize: size, weight: UIFontWeightSemibold)
    }
}
