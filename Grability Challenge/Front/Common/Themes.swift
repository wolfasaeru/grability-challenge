//
//  Themes.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/12/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit

class Themes {
    
    static let shared = Themes()
    
    static func setupAppTheme() {
        shared.setupAppTheme()
    }
    
    func setupAppTheme() {
        customizeNavigationBar()
        customizeToolbar()
    }
    
    private func customizeNavigationBar() {
        
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().barTintColor = UIColor.black.withAlphaComponent(0.5)
        UINavigationBar.appearance().titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont.semiBold(size: 17)
        ]
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
    }
    
    private func customizeToolbar() {
        
        UIToolbar.appearance().tintColor = .white
        UIToolbar.appearance().barTintColor = UIColor.black.withAlphaComponent(0.5)
    }
}
