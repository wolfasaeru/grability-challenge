//
//  HomeServiceLocator.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/9/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

class HomeServiceLocator: ServiceLocatorModule {
    
    func registerServices(serviceLocator: ServiceLocator) {

        serviceLocator.register { self.provideHomeViewController() }
    }
    
    private func provideHomeViewController() -> HomeVC {
        let viewController = HomeVC(nibName: nil, bundle: nil)
        viewController.homePresenter = HomePresenter(
            view: viewController,
            popularMovies: ServiceLocator.inject(),
            popularSeries: ServiceLocator.inject(),
            moviesFlowController: ServiceLocator.inject(),
            seriesFlowController: ServiceLocator.inject()
        )
        
        return viewController
    }
    
}
