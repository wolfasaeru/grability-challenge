//
//  HomeVC.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/9/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit

enum CollectionType: Int {
    case popularMovies = 1
    case popularSeries = 2
}

class HomeVC: BaseVC {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblPopularMovies: UILabel!
    @IBOutlet weak var btnMoreOfMovies: UIButton!
    @IBOutlet weak var popularMoviesCollection: UICollectionView!
    @IBOutlet weak var lblPopularSeries: UILabel!
    @IBOutlet weak var btnMoreOfSeries: UIButton!
    @IBOutlet weak var popularSeriesCollection: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    private var refresher: UIRefreshControl!
    
    fileprivate var movies = [Movie]() {
        didSet {
            popularMoviesCollection.reloadData()
        }
    }
    
    fileprivate var series = [Serie]() {
        didSet {
            popularSeriesCollection.reloadData()
        }
    }
    
    var homePresenter: HomePresenter!
    override var presenter: Presenter! {
        return homePresenter
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeUI()
    }
    
    private func customizeUI() {
        
        self.popularMoviesCollection.tag = CollectionType.popularMovies.rawValue
        self.popularSeriesCollection.tag = CollectionType.popularSeries.rawValue
        
        self.title = translate("Movies DB")
        
        self.lblPopularMovies.textColor = .white
        self.lblPopularMovies.font = .semiBold(size: 17)
        self.lblPopularMovies.text = translate("Movies")
        
        self.lblPopularSeries.textColor = .white
        self.lblPopularSeries.font = .semiBold(size: 17)
        self.lblPopularSeries.text = translate("Series")
        
        self.btnMoreOfMovies.titleLabel?.font = .regular(size: 13)
        self.btnMoreOfMovies.setTitleColor(.white, for: .normal)
        self.btnMoreOfMovies.setTitle("more... ", for: .normal)
        self.btnMoreOfMovies.addTarget(self, action: #selector(onBtnMoviesPressed), for: .touchUpInside)
        
        self.btnMoreOfSeries.titleLabel?.font = .regular(size: 13)
        self.btnMoreOfSeries.setTitleColor(.white, for: .normal)
        self.btnMoreOfSeries.setTitle("more... ", for: .normal)
        self.btnMoreOfSeries.addTarget(self, action: #selector(onBtnSeriesPressed), for: .touchUpInside)
        
        self.popularMoviesCollection.delegate = self
        self.popularMoviesCollection.dataSource = self
        
        self.popularSeriesCollection.delegate = self
        self.popularSeriesCollection.dataSource = self
        
        self.addRefreshControl()
        
        self.registerCells()
    }
    
    private func registerCells() {
        self.popularMoviesCollection.register(HomeMovieCell.self)
        self.popularSeriesCollection.register(HomeSerieCell.self)
    }
    
    private func addRefreshControl() {
        
        let attributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.regular(size: 12)]
        
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = .white
        self.refresher.attributedTitle = NSAttributedString(string: translate("⇣ Pull to refresh"), attributes: attributes)
        self.refresher.addTarget(self, action: #selector(refreshControlPulled), for: .valueChanged)
        
        self.scrollView.addSubview(self.refresher)
    }
    
    // MARK: - Target actions
    
    func refreshControlPulled() {
        self.refresher.endRefreshing()
        
        self.homePresenter.onRefresherPull()
    }
    
    func onBtnMoviesPressed() {
        
        self.homePresenter.onMoreOfMoviesPressed()
    }
    
    func onBtnSeriesPressed() {
        
        self.homePresenter.onMoreOfSeriesPressed()
    }
}

extension HomeVC: HomeView {
    
    func assign(_ movies: [Movie]) {
        self.movies = movies
    }
    
    func assign(_ series: [Serie]) {
        self.series = series
    }
    
    func toggleLoading(show: Bool) {

        self.activityIndicator.isHidden = !show
    }
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        guard let collectionType = CollectionType(rawValue: collectionView.tag) else { return 0 }
        
        switch collectionType {
        case .popularMovies: return self.movies.count
        case .popularSeries: return self.series.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        guard let collectionType = CollectionType(rawValue: collectionView.tag) else { return UICollectionViewCell() }
        
        switch collectionType {
        case .popularMovies: return movieCell(for: collectionView, at: indexPath)
        case .popularSeries: return serieCell(for: collectionView, at: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let collectionType = CollectionType(rawValue: collectionView.tag) else { return }
        
        switch collectionType {
        case .popularMovies:
            self.homePresenter.onMovieSelected(at: indexPath)
        case .popularSeries:
            self.homePresenter.onSerieSelected(at: indexPath)
        }
    }
    
    private func movieCell(for collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as HomeMovieCell
        cell.movie = self.movies[indexPath.row]
        
        return cell
    }
    
    private func serieCell(for collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as HomeSerieCell
        cell.serie = self.series[indexPath.row]
        
        return cell
    }
}
