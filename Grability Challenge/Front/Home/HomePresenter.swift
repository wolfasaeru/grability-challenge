//
//  HomePresenter.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/9/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import PromiseKit

protocol HomeView: BaseView {
    
    func assign(_ movies: [Movie])
    func assign(_ series: [Serie])
    func toggleLoading(show: Bool)
}

class HomePresenter: Presenter {
    
    private let view: HomeView
    private let popularMovies: PopularMoviesUseCase
    private let popularSeries: PopularSeriesUseCase
    private let moviesFlowController: MoviesFlowController
    private let seriesFlowController: SeriesFlowController
    
    private var movies = [Movie]() {
        didSet {
            self.view.assign(movies)
        }
    }
    
    private var series = [Serie]() {
        didSet {
            self.view.assign(series)
        }
    }
    
    private var requestsFinished = [Bool]()
    
    init(
        view: HomeView,
        popularMovies: PopularMoviesUseCase,
        popularSeries: PopularSeriesUseCase,
        moviesFlowController: MoviesFlowController,
        seriesFlowController: SeriesFlowController
    ) {
        self.view = view
        self.popularMovies = popularMovies
        self.popularSeries = popularSeries
        self.moviesFlowController = moviesFlowController
        self.seriesFlowController = seriesFlowController
    }
    
    func viewDidLoad() {
        self.loadData()
    }
    
    func onRefresherPull() {
        self.loadData()
    }
    
    func onMoreOfMoviesPressed() {
        self.moviesFlowController.presentMoviesListVC()
    }
    
    func onMoreOfSeriesPressed() {
        self.seriesFlowController.presentSeriesListVC()
    }
    
    func onMovieSelected(at indexPath: IndexPath) {
        
        self.moviesFlowController.presentMovieDetailVC(with: movies[indexPath.row])
    }
    
    func onSerieSelected(at indexPath: IndexPath) {
        
        self.seriesFlowController.presentSerieDetailVC(with: series[indexPath.row])
    }
    
    private func loadMovies() {
        
        self.requestsFinished.append(false)
        
        self.popularMovies.execute(isFirstPage: true)
            .then { movies -> Void in

                self.movies = movies
            }.always {
                
                self.finishedRequest()
            }.catch { error in

                self.view.showAlert(with: error)
        }
    }
    
    private func loadSeries() {
        
        self.requestsFinished.append(false)
        
        self.popularSeries.execute(isFirstPage: true)
            .then { series -> Void in
                
                self.series = series
            }.always {
                
                self.finishedRequest()
            }.catch { error in
                
                self.view.showAlert(with: error)
        }
    }
    
    private func loadData() {
        
        self.view.toggleLoading(show: true)
        
        self.view.assign([Movie]())
        self.view.assign([Serie]())
        
        self.loadMovies()
        self.loadSeries()
    }
    
    private func finishedRequest() {
        
        guard let isLoadingIndex = self.requestsFinished.index(of: false) else { return }
        
        self.requestsFinished[isLoadingIndex] = true
        
        let finished = self.requestsFinished.filter { !$0 }
        
        if finished.isEmpty {
            self.view.toggleLoading(show: false)
        }
    }
    
}
