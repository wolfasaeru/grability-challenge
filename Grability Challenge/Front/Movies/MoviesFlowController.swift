//
//  MoviesFlowController.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/12/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit

class MoviesFlowController {

    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    private var navigationViewController: UINavigationController? {
        return window.rootViewController as? UINavigationController
    }
    
    func presentMoviesListVC() {
        
        let moviesListVC = MoviesListVC(nibName: nil, bundle: nil)
        moviesListVC.moviesListPresenter = MoviesListPresenter(
            view: moviesListVC,
            moviesFlowController: ServiceLocator.inject(),
            popularMovies: ServiceLocator.inject(),
            topRatedMovies: ServiceLocator.inject(),
            upcomingMovies: ServiceLocator.inject()
        )
        
        navigationViewController?.pushViewController(moviesListVC, animated: true)
    }
    
    func presentMovieDetailVC(with movie: Movie) {
        
        let movieDetailVC = MovieDetailVC(nibName: nil, bundle: nil)
        movieDetailVC.movieDetailPresenter = MovieDetailPresenter(
            view: movieDetailVC,
            movie: movie
        )
        
        navigationViewController?.pushViewController(movieDetailVC, animated: true)
    }
    
}
