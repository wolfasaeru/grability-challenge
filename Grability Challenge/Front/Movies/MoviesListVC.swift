//
//  MoviesListVC.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/12/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit

class MoviesListVC: BaseVC {

    @IBOutlet weak var moviesCollection: UICollectionView!
    private var refresher: UIRefreshControl!
    
    fileprivate var bottomReached = false {
        didSet {
            self.moviesCollection.reloadData()
        }
    }
    
    fileprivate var movies = [Movie]() {
        didSet {
            self.moviesCollection.reloadData()
        }
    }
    
    var moviesListPresenter: MoviesListPresenter!
    override var presenter: Presenter! {
        return moviesListPresenter
    }
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        self.customizeUI()
    }

    private func customizeUI() {
        
        self.title = translate("Movies - Popular")
        
        self.moviesCollection.delegate = self
        self.moviesCollection.dataSource = self
        
        self.createToolbar()
        
        self.addRefreshControl()
        
        self.registerCells()
    }
    
    private func registerCells() {
        
        self.moviesCollection.register(MoviePreviewCell.self)
        self.moviesCollection.register(LoadingCell.self)
    }
    
    private func createToolbar() {
        
        self.toolbarHidden = false
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let btnShowPopular = UIBarButtonItem(title: translate("Popular"), style: .plain, target: self, action: #selector(showPopularPressed))
        let btnShowTopRated = UIBarButtonItem(title: translate("Top Rated"), style: .plain, target: self, action: #selector(showTopRatedPressed))
        let btnShowUpcoming = UIBarButtonItem(title: translate("Upcoming"), style: .plain, target: self, action: #selector(showUpcomingPressed))
        
        self.setToolbarItems([btnShowPopular, flexibleSpace, btnShowTopRated, flexibleSpace, btnShowUpcoming], animated: false)
    }
    
    private func addRefreshControl() {
        
        let attributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.regular(size: 12)]
        
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = .white
        self.refresher.attributedTitle = NSAttributedString(string: translate("⇣ Pull to refresh"), attributes: attributes)
        self.refresher.addTarget(self, action: #selector(refreshControlPulled), for: .valueChanged)
        
        self.moviesCollection.addSubview(self.refresher)
    }
    
    private func switchCategory() {
        
        guard !movies.isEmpty else { return }
        
        let indexPath = IndexPath(row: 0, section: 0)
        self.moviesCollection.scrollToItem(at: indexPath, at: .top, animated: false)
    }
    
    // MARK: - Target actions
    func showPopularPressed() {
        
        self.title = translate("Movies - Popular")
        self.moviesListPresenter.onShowPopularPressed()
        self.switchCategory()
    }
    
    func showTopRatedPressed() {
        
        self.title = translate("Movies - Top Rated")
        self.moviesListPresenter.onShowTopRatedPressed()
        self.switchCategory()
    }
    
    func showUpcomingPressed() {
        
        self.title = translate("Movies - Upcoming")
        self.moviesListPresenter.onShowUpcomingPressed()
        self.switchCategory()
    }
    
    func refreshControlPulled() {
        
        self.refresher.endRefreshing()
        
        self.moviesListPresenter.onRefresherPull()
    }
}

extension MoviesListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if !bottomReached && indexPath.row + 1 > movies.count {
            return CGSize(width: self.view.frame.width, height: 40)
        }
        
        return CGSize(width: 200, height: 280)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return !bottomReached ? movies.count + 1 : movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if !bottomReached && indexPath.row + 1 > movies.count {
            
            return loadingCell(for: collectionView, at: indexPath)
        }
        
        return self.movieCell(for: collectionView, at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.moviesListPresenter.onMovieSelected(at: indexPath)
    }
    
    private func movieCell(for collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as MoviePreviewCell
        cell.movie = self.movies[indexPath.row]
        
        return cell
    }
    
    private func loadingCell(for collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        
        self.moviesListPresenter.onLoadingMoreShowed()
        
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as LoadingCell
        cell.activityIndicator.startAnimating()
        
        return cell
    }
}

extension MoviesListVC: MoviesListView {
    
    func assign(movies: [Movie]) {
        self.movies = movies
    }
    
    func doesReachedBottom(_ reached: Bool) {
        self.bottomReached = reached
    }
}
