//
//  MovieDetailPresenter.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/13/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

protocol MovieDetailView: BaseView {
    
    func assign(movie: Movie)
}

class MovieDetailPresenter: Presenter {
    
    private let view: MovieDetailView
    private let movie: Movie
    
    init(
        view: MovieDetailView,
        movie: Movie
    ) {
        self.view = view
        self.movie = movie
    }
    
    func viewDidLoad() {
        
        self.view.assign(movie: movie)
    }
    
}
