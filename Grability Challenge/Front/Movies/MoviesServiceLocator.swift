//
//  MoviesServiceLocator.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/12/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

class MoviesServiceLocator: ServiceLocatorModule {
    
    func registerServices(serviceLocator: ServiceLocator) {
        
        serviceLocator.register { self.provideMoviesFlowController() }
    }
    
    private func provideMoviesFlowController() -> MoviesFlowController {

        return MoviesFlowController(window: ServiceLocator.inject())
    }
}
