//
//  MovieDetailVC.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/13/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit
import Nuke

class MovieDetailVC: BaseVC {

    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblMovieTitle: UILabel!
    @IBOutlet weak var txtOverview: UITextView!
    
    var movieDetailPresenter: MovieDetailPresenter!
    override var presenter: Presenter! {
        return movieDetailPresenter
    }
    
    fileprivate var movie: Movie! {
        didSet {
            self.fillInfo()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.customizeUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
     
        self.txtOverview.setContentOffset(.zero, animated: false)
    }
    
    private func customizeUI() {
        
        self.lblRating.textColor = .white
        self.lblRating.font = .semiBold(size: 15)
     
        self.lblMovieTitle.textColor = .white
        self.lblMovieTitle.font = .bold(size: 20)
        
        self.txtOverview.contentInset = .zero
        self.txtOverview.textContainerInset = .zero
        self.txtOverview.textColor = .white
        self.txtOverview.backgroundColor = .clear
        self.txtOverview.font = .regular(size: 14)
    }

    private func fillInfo() {
        
        self.lblRating.text = "\(self.movie.voteAvg) / 10"
        self.lblMovieTitle.text = self.movie.title
        self.txtOverview.text = self.movie.overview
        
        self.loadPosterImage()
    }
    
    private func loadPosterImage() {
        
        guard let url = URL(string: MoviesDBConfiguration.apiImageUrl)?
            .appendingPathComponent(MoviesDBConfiguration.ImgSizePath.medium.rawValue)
            .appendingPathComponent(self.movie.posterPath) else {
                return
        }
        
        Nuke.loadImage(with: Request(url: url), into: self.imgPoster)
    }
}

extension MovieDetailVC: MovieDetailView {
    
    func assign(movie: Movie) {
        self.movie = movie
    }
}
