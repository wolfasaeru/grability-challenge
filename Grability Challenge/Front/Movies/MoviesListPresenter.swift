//
//  MoviesListPresenter.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/12/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

protocol MoviesListView: BaseView {
    
    func assign(movies: [Movie])
    func doesReachedBottom(_ reached: Bool)
}

enum MoviesCategory {
    
    case popular
    case topRated
    case upcoming
}

class MoviesListPresenter: Presenter {
 
    private let view: MoviesListView
    private let moviesFlowController: MoviesFlowController
    private let popularMoviesUseCase: PopularMoviesUseCase
    private let topRatedMoviesUseCase: TopRatedMoviesUseCase
    private let upcomingMoviesUseCase: UpcomingMoviesUseCase
    
    private var disableLoading = [
        MoviesCategory.popular: false,
        MoviesCategory.topRated: false,
        MoviesCategory.upcoming: false
    ]
    
    private var showingCategory = MoviesCategory.popular
    
    private var popularMovies = [Movie]() {
        didSet {
            if showingCategory == .popular {
                
                self.view.assign(movies: popularMovies)
            }
        }
    }
    
    private var topRatedMovies = [Movie]() {
        didSet {
            if showingCategory == .topRated {
                
                self.view.assign(movies: topRatedMovies)
            }
        }
    }
    
    private var upcomingMovies = [Movie]() {
        didSet {
            if showingCategory == .upcoming {
                
                self.view.assign(movies: upcomingMovies)
            }
        }
    }
    
    init(
        view: MoviesListView,
        moviesFlowController: MoviesFlowController,
        popularMovies: PopularMoviesUseCase,
        topRatedMovies: TopRatedMoviesUseCase,
        upcomingMovies: UpcomingMoviesUseCase
    ) {
        self.view = view
        self.moviesFlowController = moviesFlowController
        self.popularMoviesUseCase = popularMovies
        self.topRatedMoviesUseCase = topRatedMovies
        self.upcomingMoviesUseCase = upcomingMovies
    }
    
    func viewDidLoad() {
        
        self.loadData()
    }
    
    func onShowPopularPressed() {
        
        self.showingCategory = .popular
        self.view.doesReachedBottom(disableLoading[.popular]!)
        self.view.assign(movies: popularMovies)
    }
    
    func onShowTopRatedPressed() {
        
        self.showingCategory = .topRated
        self.view.doesReachedBottom(disableLoading[.topRated]!)
        self.view.assign(movies: topRatedMovies)
    }
    
    func onShowUpcomingPressed() {
        
        self.showingCategory = .upcoming
        self.view.doesReachedBottom(disableLoading[.upcoming]!)
        self.view.assign(movies: upcomingMovies)
    }
    
    func onLoadingMoreShowed() {
        
        switch showingCategory {
        case .popular:
            self.loadPopular()
        case .topRated:
            self.loadTopRated()
        case .upcoming:
            self.loadUpcoming()
        }
    }
    
    func onRefresherPull() {
        
        self.loadData()
    }
    
    func onMovieSelected(at indexPath: IndexPath) {
        
        switch showingCategory {
        case .popular:
            
            self.moviesFlowController.presentMovieDetailVC(with: popularMovies[indexPath.row])
        case .topRated:
            
            self.moviesFlowController.presentMovieDetailVC(with: topRatedMovies[indexPath.row])
        case .upcoming:
            
            self.moviesFlowController.presentMovieDetailVC(with: upcomingMovies[indexPath.row])
        }
    }
    
    private func loadData() {
        
        self.view.doesReachedBottom(false)
        
        self.popularMovies = []
        self.disableLoading[.popular] = false
        self.loadPopular(reload: true)
        
        self.topRatedMovies = []
        self.disableLoading[.topRated] = false
        self.loadTopRated(reload: true)
        
        self.upcomingMovies = []
        self.disableLoading[.upcoming] = false
        self.loadUpcoming(reload: true)
    }
    
    private func loadPopular(reload: Bool = false) {
        
        popularMoviesUseCase.execute(isFirstPage: reload)
            .then { movies -> Void in

                guard self.checkNewMovies(movies) else { return }
                self.popularMovies += movies
            }.catch { error in
                
                self.view.showAlert(with: error)
        }
    }
    
    private func loadTopRated(reload: Bool = false) {
        
        topRatedMoviesUseCase.execute(isFirstPage: reload)
            .then { movies -> Void in
                
                guard self.checkNewMovies(movies) else { return }
                self.topRatedMovies += movies
            }.catch { error in
                
                self.view.showAlert(with: error)
        }
    }
    
    private func loadUpcoming(reload: Bool = false) {
        
        upcomingMoviesUseCase.execute(isFirstPage: reload)
            .then { movies -> Void in
                
                guard self.checkNewMovies(movies) else { return }
                self.upcomingMovies += movies
            }.catch { error in
                
                self.view.showAlert(with: error)
        }
    }
    
    private func checkNewMovies(_ movies: [Movie]) -> Bool {
        guard !movies.isEmpty else {
            self.disableLoading[self.showingCategory] = true
            self.view.doesReachedBottom(true)
            
            return false
        }
        
        return true
    }
}
