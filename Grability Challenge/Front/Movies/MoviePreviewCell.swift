//
//  MoviePreviewCell.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/12/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit
import Nuke

class MoviePreviewCell: UICollectionViewCell {

    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var overlay: UIView!
    @IBOutlet weak var lblMovieTitle: UILabel!
    @IBOutlet weak var lblMovieRating: UILabel!
    
    var movie: Movie! {
        didSet {
            self.fillInfo(with: movie)
        }
    }
    
    override func awakeFromNib() {
    
        super.awakeFromNib()
        
        self.customizeUI()
    }

    private func customizeUI() {
        
        self.lblMovieTitle.textColor = .white
        self.lblMovieTitle.font = .semiBold(size: 13)
        
        self.lblMovieRating.textColor = .white
        self.lblMovieRating.font = .regular(size: 11)
        
        self.gradient()
    }
    
    private func fillInfo(with movie: Movie) {
        
        self.lblMovieTitle.text = movie.title
        self.lblMovieRating.text = "\(movie.voteAvg) / 10"
        
        self.loadPosterImage()
    }
    
    private func loadPosterImage() {
        
        guard let url = URL(string: MoviesDBConfiguration.apiImageUrl)?
            .appendingPathComponent(MoviesDBConfiguration.ImgSizePath.medium.rawValue)
            .appendingPathComponent(movie.posterPath) else {
                return
        }
        
        self.imgPoster.image = nil
        Nuke.loadImage(with: Request(url: url), into: self.imgPoster)
    }
    
    private func gradient() {
        
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.locations = [0.0, 0.7, 1.0]
        let topColor = UIColor.black.withAlphaComponent(0.0)
        let middleColor = UIColor.black.withAlphaComponent(0.3)
        let bottomColor = UIColor.black.withAlphaComponent(0.7)
        gradientLayer.colors = [topColor.cgColor, middleColor.cgColor, bottomColor.cgColor]
        gradientLayer.frame = self.overlay.bounds
        
        self.overlay.layer.insertSublayer(gradientLayer, at: 0)
        
    }
}
