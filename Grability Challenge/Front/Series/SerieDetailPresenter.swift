//
//  SerieDetailPresenter.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/13/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

protocol SerieDetailView: BaseView {
    
    func assign(serie: Serie)
}

class SerieDetailPresenter: Presenter {
    
    private let view: SerieDetailView
    private let serie: Serie
    
    init(
        view: SerieDetailView,
        serie: Serie
    ) {
        self.view = view
        self.serie = serie
    }
    
    func viewDidLoad() {
        
        self.view.assign(serie: serie)
    }
    
}
