//
//  SeriesServiceLocator.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/13/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

class SeriesServiceLocator: ServiceLocatorModule {
    
    func registerServices(serviceLocator: ServiceLocator) {
        
        serviceLocator.register { self.provideSeriesFlowController() }
    }
    
    private func provideSeriesFlowController() -> SeriesFlowController {
        
        return SeriesFlowController(window: ServiceLocator.inject())
    }
}
