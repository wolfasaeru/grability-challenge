//
//  SeriesListVC.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/13/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit

class SeriesListVC: BaseVC {
    
    @IBOutlet weak var seriesCollection: UICollectionView!
    private var refresher: UIRefreshControl!
    
    fileprivate var bottomReached = false {
        didSet {
            self.seriesCollection.reloadData()
        }
    }
    
    fileprivate var series = [Serie]() {
        didSet {
            self.seriesCollection.reloadData()
        }
    }
    
    var seriesListPresenter: SeriesListPresenter!
    override var presenter: Presenter! {
        return seriesListPresenter
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.customizeUI()
    }
    
    private func customizeUI() {
        
        self.title = translate("Series - Popular")
        
        self.seriesCollection.delegate = self
        self.seriesCollection.dataSource = self
        
        self.createToolbar()
        
        self.addRefreshControl()
        
        self.registerCells()
    }
    
    private func registerCells() {
        
        self.seriesCollection.register(SeriePreviewCell.self)
        self.seriesCollection.register(LoadingCell.self)
    }
    
    private func createToolbar() {
        
        self.toolbarHidden = false
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let btnShowPopular = UIBarButtonItem(title: translate("Popular"), style: .plain, target: self, action: #selector(showPopularPressed))
        let btnShowTopRated = UIBarButtonItem(title: translate("Top Rated"), style: .plain, target: self, action: #selector(showTopRatedPressed))
        
        self.setToolbarItems([btnShowPopular, flexibleSpace, btnShowTopRated], animated: false)
    }
    
    private func addRefreshControl() {
        
        let attributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.regular(size: 12)]
        
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = .white
        self.refresher.attributedTitle = NSAttributedString(string: translate("⇣ Pull to refresh"), attributes: attributes)
        self.refresher.addTarget(self, action: #selector(refreshControlPulled), for: .valueChanged)
        
        self.seriesCollection.addSubview(self.refresher)
    }
    
    private func switchCategory() {
        
        guard !series.isEmpty else { return }
        
        let indexPath = IndexPath(row: 0, section: 0)
        self.seriesCollection.scrollToItem(at: indexPath, at: .top, animated: false)
    }
    
    // MARK: - Target actions
    func showPopularPressed() {
        
        self.title = translate("Series - Popular")
        self.seriesListPresenter.onShowPopularPressed()
        self.switchCategory()
    }
    
    func showTopRatedPressed() {
        
        self.title = translate("Series - Top Rated")
        self.seriesListPresenter.onShowTopRatedPressed()
        self.switchCategory()
    }
    
    func refreshControlPulled() {
        
        self.refresher.endRefreshing()
        
        self.seriesListPresenter.onRefresherPull()
    }
}

extension SeriesListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if !bottomReached && indexPath.row + 1 > self.series.count {
            return CGSize(width: self.view.frame.width, height: 40)
        }
        
        return CGSize(width: 200, height: 280)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return !bottomReached ? series.count + 1 : series.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if !bottomReached && indexPath.row + 1 > self.series.count {
            
            return loadingCell(for: collectionView, at: indexPath)
        }
        
        return self.movieCell(for: collectionView, at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.seriesListPresenter.onSerieSelected(at: indexPath)
    }
    
    private func movieCell(for collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as SeriePreviewCell
        cell.serie = self.series[indexPath.row]
        
        return cell
    }
    
    private func loadingCell(for collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        
        self.seriesListPresenter.onLoadingMoreShowed()
        
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as LoadingCell
        cell.activityIndicator.startAnimating()
        
        return cell
    }
}

extension SeriesListVC: SeriesListView {

    func assign(series: [Serie]) {
        self.series = series
    }
    
    func doesReachedBottom(_ reached: Bool) {
        self.bottomReached = reached
    }
}
