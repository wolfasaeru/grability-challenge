//
//  SerieDetailVC.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/13/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit
import Nuke

class SerieDetailVC: BaseVC {
    
    @IBOutlet weak var imgPoster: UIImageView!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblSerieName: UILabel!
    @IBOutlet weak var txtOverview: UITextView!
    
    var serieDetailPresenter: SerieDetailPresenter!
    override var presenter: Presenter! {
        return serieDetailPresenter
    }
    
    fileprivate var serie: Serie! {
        didSet {
            self.fillInfo()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customizeUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.txtOverview.setContentOffset(.zero, animated: false)
    }
    
    private func customizeUI() {
        
        self.lblRating.textColor = .white
        self.lblRating.font = .semiBold(size: 15)
        
        self.lblSerieName.textColor = .white
        self.lblSerieName.font = .bold(size: 20)
        
        self.txtOverview.contentInset = .zero
        self.txtOverview.textContainerInset = .zero
        self.txtOverview.textColor = .white
        self.txtOverview.backgroundColor = .clear
        self.txtOverview.font = .regular(size: 14)
    }
    
    private func fillInfo() {
        
        self.lblRating.text = "\(self.serie.voteAvg) / 10"
        self.lblSerieName.text = self.serie.name
        self.txtOverview.text = self.serie.overview
        
        self.loadPosterImage()
    }
    
    private func loadPosterImage() {
        
        guard let url = URL(string: MoviesDBConfiguration.apiImageUrl)?
            .appendingPathComponent(MoviesDBConfiguration.ImgSizePath.medium.rawValue)
            .appendingPathComponent(self.serie.posterPath) else {
                return
        }
        
        Nuke.loadImage(with: Request(url: url), into: self.imgPoster)
    }
}

extension SerieDetailVC: SerieDetailView {
    
    func assign(serie: Serie) {
        self.serie = serie
    }
}
