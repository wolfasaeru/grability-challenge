//
//  SeriesListPresenter.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/13/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

protocol SeriesListView: BaseView {
    func assign(series: [Serie])
    func doesReachedBottom(_ reached: Bool)
}

enum SeriesCategory {
    
    case popular
    case topRated
}

class SeriesListPresenter: Presenter {
    
    private let view: SeriesListView
    private let seriesFlowController: SeriesFlowController
    private let popularSeriesUseCase: PopularSeriesUseCase
    private let topRatedSeriesUseCase: TopRatedSeriesUseCase
    
    private var disableLoading = [
        SeriesCategory.popular: false,
        SeriesCategory.topRated: false
    ]
    
    private var showingCategory = SeriesCategory.popular
    
    private var popularSeries = [Serie]() {
        didSet {
            if showingCategory == .popular {
                
                self.view.assign(series: popularSeries)
            }
        }
    }
    
    private var topRatedSeries = [Serie]() {
        didSet {
            if showingCategory == .topRated {
                
                self.view.assign(series: topRatedSeries)
            }
        }
    }
    
    init(
        view: SeriesListView,
        seriesFlowController: SeriesFlowController,
        popularSeries: PopularSeriesUseCase,
        topRatedSeries: TopRatedSeriesUseCase
    ) {
        self.view = view
        self.seriesFlowController = seriesFlowController
        self.popularSeriesUseCase = popularSeries
        self.topRatedSeriesUseCase = topRatedSeries
    }
    
    func viewDidLoad() {
        
        self.loadData()
    }
    
    func onShowPopularPressed() {
        
        self.showingCategory = .popular
        self.view.doesReachedBottom(disableLoading[.popular]!)
        self.view.assign(series: popularSeries)
    }
    
    func onShowTopRatedPressed() {
        
        self.showingCategory = .topRated
        self.view.doesReachedBottom(disableLoading[.topRated]!)
        self.view.assign(series: topRatedSeries)
    }
    
    func onLoadingMoreShowed() {
        
        switch showingCategory {
        case .popular:
            self.loadPopular()
        case .topRated:
            self.loadTopRated()
        }
    }
    
    func onRefresherPull() {
        
        self.loadData()
    }
    
    func onSerieSelected(at indexPath: IndexPath) {
        
        switch showingCategory {
        case .popular:
            
            self.seriesFlowController.presentSerieDetailVC(with: popularSeries[indexPath.row])
        case .topRated:
            
            self.seriesFlowController.presentSerieDetailVC(with: topRatedSeries[indexPath.row])
        }
    }
    
    private func loadData() {
        
        self.view.doesReachedBottom(false)
        
        self.popularSeries = []
        self.disableLoading[.popular] = false
        self.loadPopular(reload: true)
        
        self.topRatedSeries = []
        self.disableLoading[.topRated] = false
        self.loadTopRated(reload: true)
    }
    
    private func loadPopular(reload: Bool = false) {
        
        popularSeriesUseCase.execute(isFirstPage: reload)
            .then { series -> Void in
                
                guard self.checkNewSeries(series) else { return }
                self.popularSeries += series
            }.catch { error in
                
                self.view.showAlert(with: error)
        }
    }
    
    private func loadTopRated(reload: Bool = false) {
        
        topRatedSeriesUseCase.execute(isFirstPage: reload)
            .then { series -> Void in
                
                guard self.checkNewSeries(series) else { return }
                self.topRatedSeries += series
            }.catch { error in
                
                self.view.showAlert(with: error)
        }
    }
    
    private func checkNewSeries(_ series: [Serie]) -> Bool {
        guard !series.isEmpty else {
            self.disableLoading[self.showingCategory] = true
            self.view.doesReachedBottom(true)
            
            return false
        }
        
        return true
    }
}
