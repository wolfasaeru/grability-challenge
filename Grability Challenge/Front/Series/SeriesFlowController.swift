//
//  SeriesFlowController.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/13/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit

class SeriesFlowController {
    
    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    private var navigationViewController: UINavigationController? {
        return window.rootViewController as? UINavigationController
    }
    
    func presentSeriesListVC() {
        
        let seriesListVC = SeriesListVC(nibName: nil, bundle: nil)
        seriesListVC.seriesListPresenter = SeriesListPresenter(
            view: seriesListVC,
            seriesFlowController: ServiceLocator.inject(),
            popularSeries: ServiceLocator.inject(),
            topRatedSeries: ServiceLocator.inject()
        )
        
        navigationViewController?.pushViewController(seriesListVC, animated: true)
    }
    
    func presentSerieDetailVC(with serie: Serie) {
        
        let serieDetailVC = SerieDetailVC(nibName: nil, bundle: nil)
        serieDetailVC.serieDetailPresenter = SerieDetailPresenter(
            view: serieDetailVC,
            serie: serie
        )
        
        navigationViewController?.pushViewController(serieDetailVC, animated: true)
    }
}
