//
//  AppDelegate.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/9/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import UIKit
import CoreData
import JSQCoreDataKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        ServiceLocator.registerModules()
        Themes.setupAppTheme()

        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white
        
        CoreDataConfiguration.setupStack().always {
            let rootViewController: HomeVC = ServiceLocator.inject()
            let navigationController = ChallengeNavigationController(rootViewController: rootViewController)
            
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }

}
