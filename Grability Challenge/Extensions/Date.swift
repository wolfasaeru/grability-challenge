//
//  Date.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/11/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation

extension Date {
    
    static func fromApiDate(with strDate: String) -> Date {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        guard let date = formatter.date(from: strDate) else { return Date() }
        
        return date
    }
    
}
