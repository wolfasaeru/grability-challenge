//
//  SystemServiceLocator.swift
//  Grability Challenge
//
//  Created by Hasael Oliveros on 8/9/17.
//  Copyright © 2017 Hasael Oliveros. All rights reserved.
//

import Foundation
import UIKit

class SystemServiceLocator: ServiceLocatorModule {
    
    func registerServices(serviceLocator: ServiceLocator) {
        serviceLocator.register { self.mainWindow }
    }
    
    private var mainWindow: UIWindow {
        return UIApplication.shared.windows.first!
    }
}
